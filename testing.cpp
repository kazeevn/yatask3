#include "hmm.h"
#include "gtest/gtest.h"

namespace {
class HMMTest:public::testing::Test {};
TEST_F(HMMTest, simple_viterbi) {
  HMM hmm = HMM();
  hmm.init_from_file("machines/simple");
  Eigen::Vector4i out_second(1,1,1,1);
  Eigen::Vector4i states(1,1,1,1);
  Eigen::VectorXi* res = hmm.viterbi(out_second);
  ASSERT_EQ(states, *res);
  delete res;
}

TEST_F(HMMTest, reading_vector) {
  Eigen::Vector2f correct(0.5, 0.5);
  Eigen::VectorXf* res = read_vector("machines/simple/starting");
  ASSERT_EQ((*res).size(), correct.size());
  ASSERT_EQ(*res, correct);
  delete res;
}

TEST_F(HMMTest, unexistant_vector) {
  ASSERT_ANY_THROW(read_vector("machisdfgdghbhr"));
}

TEST_F(HMMTest, reading_matrix) {
  Eigen::Matrix2f correct;
  correct << 1,0,0,1;
  Eigen::MatrixXf* res = read_matrix("machines/simple/states", 2);
  ASSERT_EQ((*res).size(), correct.size());
  ASSERT_EQ(*res, correct);
  res = read_matrix("machines/simple/emission", 2);
  ASSERT_EQ((*res).size(), correct.size());
  ASSERT_EQ(*res, correct);
  delete res;
}

TEST_F(HMMTest, nonexistent_matrix) {
  ASSERT_ANY_THROW(read_matrix("ergergh4hg45th", 123));
}

TEST_F(HMMTest, real_data_load) {
  Eigen::MatrixXf correct(3,2);
  correct << 0.005, 0.604, 0.775, 0.277, 0.220, 0.119;
  Eigen::MatrixXf* res = read_matrix("machines/task/emission", 2);
  ASSERT_EQ((*res).size(), correct.size());
  ASSERT_EQ(*res, correct);
}

TEST_F(HMMTest, some_real_data_viterbi) {
  HMM hmm = HMM();
  hmm.init_from_file("machines/task");
  Eigen::VectorXi obs(7); obs << 1, 1, 1, 1, 2, 1, 1;
  Eigen::VectorXi states(7); states << 0,0,0,0,0,0,0;
  Eigen::VectorXi* res = hmm.viterbi(obs);
  ASSERT_EQ(states.size(), (*res).size());
  ASSERT_EQ(states, *res);
  delete res;
}

TEST_F(HMMTest, all_real_data_viterbi) {
  HMM hmm = HMM();
  hmm.init_from_file("machines/task");
  Eigen::VectorXi* obs = read_vector_i("data-in-num.data");
  Eigen::VectorXi* states = read_vector_i("etalon-viterbi.res");
  Eigen::VectorXi* res = hmm.viterbi(*obs);
  ASSERT_EQ((*states).size(), (*res).size());
  ASSERT_EQ(*states, *res);
  delete res;
}
} // namespace
