hmm: hmm.cpp main.cpp hmm.h
	g++ --std=c++0x -O2 -Wall -Werror -o hmm hmm.cpp main.cpp

run_tests: testing.cpp hmm.h hmm.cpp
	g++ --std=c++0x -g -O0 -Wall -Werror -lgtest_main -lgtest -pthread -o run_tests testing.cpp hmm.cpp

test: run_tests
	./run_tests
