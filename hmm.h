// Copyright 2013, Nikita Kazeev
#ifndef HMM_H
#define HMM_H
#include <string.h>
#include <eigen3/Eigen/Dense>

class HMM {
public:
  HMM();
  // The dir must contain emission, starting and states
  void init_from_file(const std::string dirname);
  Eigen::VectorXi* viterbi(const Eigen::VectorXi observations);
private:
  int n_states, n_observations;
  Eigen::VectorXf* initial_p;
  Eigen::MatrixXf* state_transition_p;
  Eigen::MatrixXf* observation_p;
};

Eigen::VectorXi* read_vector_i(const std::string filename);
Eigen::VectorXf* read_vector(const std::string filename);
Eigen::MatrixXf* read_matrix(const std::string filename, const int col_number);
#endif
