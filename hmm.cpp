#include "hmm.h"
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>

HMM::HMM() {};

Eigen::VectorXi* read_vector_i(const std::string filename) {
  /* Reads vector of floats from the file, return Eigen vector */
  std::ifstream inFile(filename);
  std::istream_iterator<int> inIter = std::istream_iterator<int>(inFile);
  std::istream_iterator<int> eos;

  int size = 0;
  Eigen::VectorXi* the_vector = new Eigen::VectorXi;
  int cur_size = 0;
  while (inIter != eos) {
    if (cur_size >= size) {
      size += 10;
      the_vector->conservativeResize(size);
    }
    (*the_vector)[cur_size] = *inIter;
    inIter++;
    cur_size++;
  }
  if (cur_size == 0) {
    throw "Error reading vector from " + filename;
  }
  the_vector->conservativeResize(cur_size);
  inFile.close();
  return the_vector;
}

Eigen::VectorXf* read_vector(const std::string filename) {
  /* Reads vector of floats from the file, return Eigen vector */
  std::ifstream inFile(filename);
  std::istream_iterator<float> inIter = std::istream_iterator<float>(inFile);
  std::istream_iterator<float> eos;

  int size = 0;
  Eigen::VectorXf* the_vector = new Eigen::VectorXf;
  int cur_size = 0;
  while (inIter != eos) {
    if (cur_size >= size) {
      size += 10;
      the_vector->conservativeResize(size);
    }
    (*the_vector)[cur_size] = *inIter;
    inIter++;
    cur_size++;
  }
  if (cur_size == 0) {
    throw "Error reading vector from " + filename;
  }
  the_vector->conservativeResize(cur_size);
  inFile.close();
  return the_vector;
}

Eigen::MatrixXf* read_matrix(const std::string filename, const int col_number) {
  /* Reads a matrix from the file. It is assumed to have col_number columns */
  std::ifstream inFile(filename);
  int n_rows = 0;
  int cur_n_rows = 0;
  Eigen::MatrixXf* the_matrix = new Eigen::MatrixXf;

  while (inFile.good()) {
    std::string line;
    std::getline(inFile, line);
    std::istringstream iline(line);
    std::istream_iterator<float> iter_line(iline);
    std::istream_iterator<float> eos;
    if (cur_n_rows >= n_rows) {
      n_rows += 10;
      the_matrix->conservativeResize(n_rows, col_number);
    }
    int i = 0;
    for (iter_line = iter_line; iter_line != eos; ++iter_line) {
      (*the_matrix)(cur_n_rows, i) = *iter_line;
      ++i;
    }
    // Check against comments and other broken lines
    if (i==col_number)
      ++cur_n_rows;
  }
  if (cur_n_rows == 0) {
    throw "Error reading matrix from " + filename;
  }
  the_matrix->conservativeResize(cur_n_rows, col_number);
  inFile.close();
  return the_matrix;
}

void HMM::init_from_file(const std::string dirname) {
  initial_p = read_vector(dirname + "/starting");
  n_states = initial_p->size();
  state_transition_p = read_matrix(dirname + "/states", n_states);
  observation_p = read_matrix(dirname + "/emission", n_states);
  n_observations = observation_p->rows();
}

Eigen::VectorXi* HMM::viterbi(const Eigen::VectorXi observations) {
  int T = observations.size();
  Eigen::MatrixXi path(T, n_states);
  Eigen::MatrixXf V(T, n_states);

  // Initialize t=0 probabilities
  for (int state = 0; state < n_states; ++state) {
    V(0, state) = log((*initial_p)[state]) + log((*observation_p)(observations[0], state));
    path(0, state) = state;
  }
  // Viterbi
  for (int t = 1; t < T; ++t) {
    Eigen::MatrixXi new_path(T, n_states);
    for (int state = 0; state < n_states; ++state) {
      float prob = -1e100;
      int best_state = -1;
      for (int state2 = 0; state2 < n_states; ++state2) {
        float cur_prob = V(t-1, state2) + log((*state_transition_p)(state, state2)) +
            log((*observation_p)(observations[t], state));
        if (cur_prob >= prob) {
          prob = cur_prob;
          best_state = state2;
        }
      }
      V(t, state) = prob;
      new_path.col(state) = path.col(best_state);
      new_path(t, state) = state;
    }
    path = new_path;
  }

  // Now we look for the most probable path
  Eigen::MatrixXf::Index res_state;
  // float res_prob = V.row(T-1).maxCoeff(&res_state);
  V.row(T-1).maxCoeff(&res_state);
  Eigen::VectorXi* res = new Eigen::VectorXi;
  (*res) = path.col(res_state);
  // std::cout << res_prob << "\n";
  return res;
}
