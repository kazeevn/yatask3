library("HMM")
transP <- as.matrix(read.table("machines/task/states"))
emisP <- t(as.matrix(read.table("machines/task/emission")))
startP <- as.matrix(read.table("machines/task/starting"))
modellingData <- read.table("hmmdata", header=TRUE)
observations <- as.matrix(modellingData[3])[,1]

hmm = initHMM(c("0", "1"), c("a","b","c"),
  transProbs=transP, emissionProbs=emisP, startProbs = startP)
res = viterbi(hmm, observations)
write(res, "etalon-viterbi.res")

numFormA = gsub("a", "0", observations)
numFormB = gsub("b", "1", numFormA)
numFormC = gsub("c", "2", numFormB)
write(numFormC, "data-in-num.data")

